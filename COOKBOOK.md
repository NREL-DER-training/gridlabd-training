Goals
=====

Provide helpful hints for working with the hands-on feeder and
integration study projects

Setup
=====

This cookbook assumes GridLAB-D v3.2, Python 3 with conda and
programmer oriented text editor (e.g. TextWrangler, Notepad++ or Sublime Text) are all
installed. See [INSTALL](./INSTALL.md) notes for details.

1. Open a command prompt or terminal  
1. Create a Softwares folder if it doesn't already exist.
    - Windows
        - `cd C:\DER-training\`
    - OSX
        - `cd ~/DER-training`
        
1. Update `pyder`
    - `cd PythonDER`
    - `git pull`
    - `cd ..`
1. Type `git clone https://gitlab.com/NREL-DER-training/gridlabd-training.git`
1. Change to the folder 
    - `cd gridlabd-training`
1. Find the input folder here and change directories to the input folder 
    - `cd input`

Contents
========

* Goals 
* Setup
* Contents
* Basic Tasks
* Run the snapshot (static) GridLAB-D model
* Run the time series model
* Plot the test feeder topology
* Plot the feeder voltage profile
* Adding Solar PV
* The GridLAB-D Models
* Output Files
* Data Links
* IEEE Test Feeders
* PG&E Load Profiles
* Solar Prospector Solar Data
* In My Backyard
* Additional Help Resources
* On-line GridLAB-D documentation:
* GridLAB-D Parameter help from the command line:
* Quick start guide for Python

# Basic Tasks

Test if you have `gridlabd` installed

    gridlabd --version

Test if you have `pyder` installed

    pyder --version


# Run the snapshot (static) GridLAB-D model

`gridlabd IEEE123_static_main.glm`

# Run the time series model

`gridlabd IEEE123_dynamic_main.glm`

# Plot the test feeder topology

Check if `pyder` is installed by running `pyder --version`

To display to the screen with the default display terminal:

    pyder plot --input-data ./IEEE123_BP-nodes.csv
 
**Hint** : Try adding `--labels` to the previous command line. Has the output changed? Now try `--font-size=20`

# Plot the feeder voltage profile

    pyder plot --input-data ./IEEE123_BP-nodes.csv --voltage-data output/ieee123_Vdump.csv

**Hint** : Try adding `--labels` to the previous command. Try `pyder plot --help` for the list of entire commands.

# Adding Solar PV

Create a `solar_input.csv` file in the current folder. See `data/solar_method2.csv` as an example.

| name | parent   | phases | nominal_voltage | INVERTER_generator_status | INVERTER_inverter_efficiency | INVERTER_generator_mode | INVERTER_inverter_type | INVERTER_power_factor | INVERTER_rated_power | SOLAR_generator_mode | SOLAR_generator_status | SOLAR_panel_type       | SOLAR_efficiency | SOLAR_area | 
|------|----------|--------|-----------------|---------------------------|------------------------------|-------------------------|------------------------|-----------------------|----------------------|----------------------|------------------------|------------------------|------------------|------------| 
| 300  | node_300 | ABCN   | 2401.7771       | ONLINE                    | 0.95                         | CONSTANT_PF             | PWM                    | 1.0                   | 2.4MW                | SUPPLY_DRIVEN        | ONLINE                 | SINGLE_CRYSTAL_SILICON | 0.2              | 20000 m^2  | 


We are setting up an input file to add solar to node 300 on phases A,B and C. Now let's run the `pyder add-solar` to create a `der/IEEE123_pv.glm` file. 

    pyder add-solar --is-solar --input-data solar_input.csv --output der/IEEE123_pv.glm


Run `gridlabd IEEE123_static_main.glm` again, and plot the results.

    pyder plot --input-data ./IEEE123_BP-nodes.csv --voltage-data output/ieee123_Vdump.csv --labels --voltage-labels --font-size=10 --node-size=200

What does the voltage profile look like now?

# Adjusting System Voltage Controls

Edit IEEE123\_controls.glm with a text editor.

### Output Files

Net power at the substation `ieee123_net_pwr.csv`

Total solar `ieee123_pv_pwr.csv`

# Data Links

### IEEE Test Feeders

Look for both the paper with overview and configuration data plus the
specific test feeder data:

<http://www.ewh.ieee.org/soc/pes/dsacom/testfeeders/index.html>

### PG&E Load Profiles

Hourly data for typical customers, by rate class from 2001 until
\~yesterday. The other major CA utilities have similar data available.

“Dynamic”, updated in near real time for residential, commercial: Also
has a link for the translation table

<http://www.pge.com/tariffs/energy_use_prices.shtml>

“Static”, updated a little less often for agriculture, street lights,
etc.:

<http://www.pge.com/nots/rates/006f1c4_class_load_prof.shtml>

### Solar Prospector Solar Data

Download historic hourly weather data in TMY2 (including real time
series. i.e., NOT a fictitious “typical” year) or CSV format. Data from
1998-2009. Many interesting map layers.

<http://maps.nrel.gov/prospector>

### In My Backyard

Drag on your roof and estimate the PV production and economics.

<http://maps.nrel.gov/imby>


Additional Help Resources
=========================

### On-line GridLAB-D documentation: 

Use the wiki at: In addition to these contents, you can use the wiki
search feature

<http://sourceforge.net/apps/mediawiki/gridlab-d/index.php?title=Help:Contents>

Be aware that google searches may turn up links to the now out of date
doxygen based code documentation. Look for the wiki documentation for
updated information.

GridLAB-D Parameter help from the command line:
-----------------------------------------------

Get a list of object parameters, parents, and types using gridLAB’s
modhelp. E.g. for information on node objects:

gridlabd --modhelp powerflow:node


Quick start guide for Python
----------------------------

Geared toward folks already comfortable with a different programming
language.

<http://www.diveintopython.net/>