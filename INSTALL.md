Goals
=====

Setup and install GridLAB and related tools

Contents
========

- Goals
- Contents
- Install git (for code version control)
- Install GridLAB-D
- Install a programming oriented text editor
- Install text editor
- Check and Install Python

# Install `git`

* Windows 
    * Download latest verison of `git` (2.8.3 x64) from <https://git-scm.com/downloads>
    * Ensure `Git Bash Here` is selected (default)
    * Ensure `Git GUI Here` is selected (default)
    * Select `Use Git from the Windows Command Prompt` (default)
    * Select `Checkout Windows-style, commit Unix-style line endings` (default)
    * Select `Use MinTTY (the default terminal of MSYS2)` (default)
    * Hit Next for remaining
* OSX 
    * Download latest version of `git` (2.8.1) from <https://git-scm.com/downloads>
    * If you get the warning about the security preferences you can right click (or control click) on gridlabd.mpkg and press open _OR_ go to 
    System Preferences > Security & Privacy > General > Allow applications downloaded from > Anywhere
    * If using git for the first time from terminal, you will get a prompt to say GIT not installed do you want to download? Select yes (you don’t need the full Xcode)

# Install GridLAB-D

### Windows

*IMPORTANT: You have to specify some custom installation options*

1.  Download the GridLAB-D v3.2 installer from
    <http://sourceforge.net/projects/gridlab-d/files/gridlab-d/>.
    There should be a link labeled “Looking for the latest
    version?” located above the table. 
    **WARNING**: Be careful when downloading this and other
    installer files. Once you select a file to download the 
    should START AUTOMATICALLY after about a 5-10sec delay. Often the
    download page is filled with misleading ads with large, colorful
    download links. Annoying, but this must be how they pay the bills.
    Read the surrounding text carefully before clicking on
    download links.

2.  Run the installer[^4]. On the “select components” page, scroll down
    and select “download and install MinGW.” You may optionally also
    choose to install some or all of the climate data files (recommended
    for future work, not used in this class). 

3.  On the “Select Additional Tasks” be sure (at least) the options to
    add GridLAB-D to the PATH, to create GLPATH, and to create GLTEMP
    are all selected.

4.  Verify the install from the Command Prompt[^5] by typing
    `gridlabd --version`. It should display something like: 

        GridLAB-D 3.2.0-5368 (Jojoba) 64-bit WINDOWS RELEASE

### OSX

1.  Download and install GridLAB-D v3.2 from
    <http://sourceforge.net/projects/gridlab-d/files/gridlab-d/>. 
    There should be a link labeled “Looking for the latest 
    version?” located above the table. 
    **WARNING**: Be careful when downloading this and
    other installer files. Once you select a file to download the
    download should START AUTOMATICALLY after about a 5-10sec delay.
    Often the download page is filled with misleading ads with large,
    colorful download links. Annoying, but this must be how they pay
    the bills. Read the surrounding text carefully before clicking on
    download links.

2. This should  open an installer window that you can click through with the default options. Troubleshooting:
    * Look behind other windows if the installer window is not visible.
    * If you get the warning about the security preferences you can right click (or control click) on `gridlabd.mpkg` and press Open _-OR-_ permantly allow such exceptions (careful) using System Preferences > Security & Privacy > General > Allow applications downloaded from > Anywhere
    * Currently GridLAB-D (v. 3.2.0-5368) will not install under El Capitan (OSX 10.11). See https://sourceforge.net/p/gridlab-d/discussion/842562/thread/a83598f8/#9824 for a possible work around. If you go this route, it is recommened to be sure to reenable SIP for security reasons.

3.  Verify the install in Terminal[^1] by typing `gridlabd --version`. It
    should display something like: 
        
        GridLAB-D 3.2.0-5368 (Jojoba) 64-bit MACOSX RELEASE.


# Install a programming oriented text editor (feel free to use your favorite)

*GridLAB-D makes extensive use of text files, so you will want a good
quality, programmer oriented text editor. Feel free to substitute your
favorite alternative editor.*

_Select C++ file type for reasonable syntax highlighting_

### Option 1: (RECOMMENDED)

**Windows**
 1. Download and install [NotePad++](https://notepad-plus-plus.org/download/)
 
**OSX**
 1. Download and install [TextWrangler](http://www.barebones.com/products/textwrangler/download.html)

### Option 2: Sublime Text (alternative)
1.  Download and install Sublime Text 3 from <https://www.sublimetext.com/3>
2.  (Optional) Right click at the bottom and select C++ for syntax highlighting in Sublime


# Install Python

**Option 1 : Using Anaconda (RECOMMENDED)**

_Note: if you have previously installed Anaconda use `conda update anaconda` from the command line_

1. Download the latest version of Anaconda2 x64 from Continuum <https://www.continuum.io/downloads>. You don't have to enter any contact information if you don't want to.
    * Windows
        * Click on Anaconda2-4.0.0-Windows-x86_64.exe and follow instructions
        
    * OSX
        * Click on Anaconda2-4.0.0-MacOSX-x86_64.dmg and follow instructions

2. Verify that conda has been installed
    * `conda --version` should result in `conda 4.0.X`  
    * Windows : pip --version should result in something like `pip 8.1.X from C:\Anaconda2\lib\site-packages (python 2.7)`
    * OSX : `pip --version` should result in something like `pip 8.1.X from /Users/$USER/anaconda/lib/python2.7/site-packages (python 2.7)`
    

Do not proceed if you do not see `anaconda` in the path when you type `pip --version`. If you do not see anaconda, reinstall anaconda and check if the installation was successful. If you still do not see anaconda in the path, check your `$PATH` variable to see if the directory to anaconda exists.

**Option 2 : Using (System) Python (Advanced*)**

1. Download the latest version of Python2 from here <https://www.python.org/downloads/> i.e. Python 2.7.11
2. Follow instructions to install
3. Note that this is standard Python, and does not come with binaries for matplotlib, numpy, scipy, pandas etc. If you know how to install these then go ahead. Check advanced instructions for more information


# Install `pyder`

### Step 1 - Get `pyder`

* It is best to download `pyder` using `git`. Use the steps below or get it manually from [here](https://gitlab.com/NREL-DER-training/PythonDER.git).

Open a command line or terminal and type the following.
    
**Windows**

    mkdir C:\DER-training\
    cd C:\DER-training\
    git clone https://gitlab.com/NREL-DER-training/PythonDER.git

**OSX**

    mkdir ~/DER-training/
    cd ~/DER-training/
    git clone https://gitlab.com/NREL-DER-training/PythonDER.git
    
    
### Step 2 - Install `pyder`

To install `pyder`, we will use `conda`'s package manager. `conda` comes with a version of `pip`, which we can use to install `pyder`.

Once you have installed anaconda successfully (see above), follow the instructions below.

**Option 1 : Using conda with virtual environment (RECOMMENDED)**

1. Open command line
    - Windows : Hit Windows key, type `cmd` and hit enter
    - OSX : Hit cmd + Space combo to open spotlight, type `Terminal` and hit enter
1. Change directory to pyder
    - Windows : `cd C:\DER-training\PythonDER\`
    - OSX : `cd ~/DER-training/PythonDER/`
1. Type `conda create -n pyder-env python=3`. This will create a virtual environment.
    - If the above command does not work, you might have you run `conda config --set ssl_verify false`. Try the above command again.
1. Source the environment. This will put us in the environment. This will allow us to install python packages without affecting the system packages.
    - Windows : `activate pyder-env`
    - OSX : `source activate pyder-env`
    Once you activate the environment, you will see `(pyder-env)` before your command line
1. Type `conda install matplotlib`
1. Type `conda install pandas`
1. Type `pip install -e .` (Ensure you are in the PythonDER folder)
1. Type `pyder --version`

1. If you've already done the above steps and closed your terminal, you only need to activate the environment to run `pyder`. You can do that by running 
    - Windows : `activate pyder-env`
    - OSX : `source activate pyder-env`

**Option 2 : Using conda without virtual environments (not recommended)**

1. Open command line
    - Windows : Hit Windows key, type `cmd` and hit enter
    - OSX : Hit cmd + Space combo to open spotlight, type `Terminal` and hit enter
1. Change directory to pyder
    - Windows : `cd C:\DER-training\PythonDER\`
    - OSX : `cd ~/DER-training/PythonDER/`
1. Type `conda install matplotlib`
1. Type `conda install pandas`
1. Type `pip install -e .` (Ensure you are in the PythonDER folder)
1. Type `pyder --version`

**Option 3 : Without conda (not recommended)**

1. Open command line
    - Windows : Hit Windows key, type `cmd` and hit enter
    - OSX : Hit cmd + Space combo to open spotlight, type `Terminal` and hit enter
1. Change directory to pyder
    - Windows : `cd C:\DER-training\PythonDER\`
    - OSX : `cd ~/DER-training/PythonDER/`
1. Type `pip install matplotlib`
1. Type `pip install pandas`
1. Type `pip install -e .` (Ensure you are in the PythonDER folder)
1. Type `pyder --version`


If everything works, you should see `pyder, version 1.0.0`. Run the following to generate a plot 

     pyder plot --input-data data/test-nodes.csv --voltage-data data/voltage_dump.csv --node-size=5000 --labels --font-size=25

In the current version the plot will be in a pdf file, which you can open from the command line with `open plot_phaseA.pdf` or through Windows Explorer or Finder.

Footnotes
---------

[^1]: An easy way to start the terminal is to press Command-space and
    then start typing Terminal in the spotlight search box. It will most
    likely be the first hit.

[^4]: NREL users will have to locate the file in Explorer and right
    click to select “NREL Run as Administrator” for this and all other
    installers. Note that the NREL run as administrator is different
    than the similarly named windows provided “Run as administrator”

[^5]: An easy way to start a command prompt under Windows 7 is through
    the Start Menu|Search for programs and files box. Type cmd and it
    should appear. Under Vista/XP, use the “Run…” option under the start
    menu and type cmd.
